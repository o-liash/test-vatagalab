﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterZone : MonoBehaviour {

	private Renderer rend;

	void OnTriggerEnter(Collider other) {
		foreach (Renderer rend in GetComponentsInChildren<Renderer> ()) {
			rend.material.color = Color.green;
		}
	}

	void OnTriggerExit(Collider other) {
		foreach (Renderer rend in GetComponentsInChildren<Renderer> ()) {
			rend.material.color = Color.red;
		}
	}
}