﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetExtState : MonoBehaviour {

	public bool isScratched;
	public bool isCubeGreen;
	public bool isRandom;
	public Material[] materials;
	private Renderer rend;

	void Start () {
		setIsScratched ();
		setCubeColor ();
	}

	private void setIsScratched() {
		Renderer rend = GetComponent<Renderer> ();
		rend.enabled = true;
		if (isScratched) {
			rend.sharedMaterial = materials [1];
		} else if (isRandom) {
			rend.sharedMaterial = materials [Random.Range (0, materials.Length - 1)];
		}
	}

	private void setCubeColor() {
		Renderer[] rends = GetComponentsInChildren<Renderer> ();
		if (isCubeGreen) {
			rends[1].material.color = Color.green;
		} else if (isRandom) {
			Color[] colors = { Color.red, Color.green };
			rends[1].material.color = colors[Random.Range(0, colors.Length)];
		}
	}
}
