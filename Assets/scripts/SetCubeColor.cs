﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetCubeColor : MonoBehaviour {

	public bool isGreen;
	public bool isRandom;
	Renderer rend;

	void Start () {
		rend = GetComponent<Renderer> ();
		if (isGreen) {
			rend.material.color = Color.green;
		} else if (isRandom) {
			Color[] colors = { Color.red, Color.green };
			rend.material.color = colors[Random.Range(0, colors.Length)];
		} else {
			rend.material.color = Color.red;
		}
	}
}
