﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManipulateMenu : MonoBehaviour {

	public GameObject menu;
	private bool isShowing;

	void Start () {
		menu.SetActive (false);
	}
	
	void Update () {
		if (Input.GetKeyDown(KeyCode.C)) {
			isShowing = !isShowing;
			menu.SetActive (isShowing);
		}
		if (isShowing) {
			if (Input.GetKeyDown(KeyCode.KeypadEnter)) {
				 Debug.Log (
					areAllInPlace () + " " +
					areAllGoodCond () + " " +
					areCubesGreen () + " " +
					isCubeFixed ()
				); 
				setToggleIsOn(getToggles ("place"), areAllInPlace());
				setToggleIsOn(getToggles ("good"), areAllGoodCond());
				setToggleIsOn(getToggles ("green"), areCubesGreen());
				setToggleIsOn(getToggles ("fixed"), isCubeFixed());
			}
		}
	}

	private bool areAllInPlace() {
		GameObject[] zones = GameObject.FindGameObjectsWithTag ("zone");
		foreach (GameObject zone in zones) {
			if (zone.GetComponent<Renderer> ().material.color != Color.green) {
				return false;
			}
		}
		return true;
	}

	private bool areAllGoodCond() {
		GameObject[] exts = GameObject.FindGameObjectsWithTag ("ext");
		foreach (GameObject ext in exts) {
			Renderer rend = ext.GetComponent<Renderer> ();
			if (rend.sharedMaterials[0].name != "ext_standard") {
				return false;
			}
		}
		return true;
	}

	private bool areCubesGreen() {
		GameObject[] extcubes = GameObject.FindGameObjectsWithTag ("extcube");
		foreach (GameObject extcube in extcubes) {
			if (extcube.GetComponentInChildren<Renderer>().material.color != Color.green) {
				return false;
			}
		}
		return true;
	}

	private bool isCubeFixed() {
		GameObject ceilcube = GameObject.FindGameObjectWithTag ("ceilcube");
		if (ceilcube.transform.rotation.z != 0) {
			return false;
		}
		return true;
	}

	private void setToggleIsOn(UnityEngine.UI.Toggle[] toggles, bool isOn) {
		foreach (UnityEngine.UI.Toggle toggle in toggles) {
			toggle.isOn = isOn;
			UnityEngine.UI.ColorBlock colorBlock = toggle.colors;
			colorBlock.normalColor = isOn ? Color.green : Color.red;
			toggle.colors = colorBlock;
		}
	}

	private UnityEngine.UI.Toggle[] getToggles(string toggleName) {
		GameObject[] toggleObjects = GameObject.FindGameObjectsWithTag (toggleName);
		UnityEngine.UI.Toggle[] toggles = new UnityEngine.UI.Toggle[toggleObjects.Length];
		int count = 0;
		foreach (GameObject toggleObject in toggleObjects) {
			toggles [count] = toggleObject.GetComponent<UnityEngine.UI.Toggle> ();
			count++;
		}
		return toggles;
	}
}