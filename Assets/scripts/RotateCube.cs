﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class RotateCube : MonoBehaviour {

	public static bool isPressedF = false;
	private Transform cubeToRotate;
	private bool haveObject;
	public LayerMask mask;
	public float interactDistance = 3;

	void FixedUpdate() {
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
		if (Physics.Raycast (ray, out hit, interactDistance, mask)) {
			Debug.DrawLine (ray.origin, hit.point);
			if (Input.GetKeyDown (KeyCode.F)) {
				isPressedF = !isPressedF;
			}
		}

		if (isPressedF) {
			cubeToRotate = hit.transform;
			if (cubeToRotate != null) {
				cubeToRotate.GetComponent<Rigidbody> ().MoveRotation (Quaternion.Euler (0, 0, 45));
			}
		} else {
			if (cubeToRotate != null) {
				cubeToRotate.transform.DORotate (new Vector3 (0, 0, 0), 1);
				cubeToRotate = null;
			}
		}
	}
}