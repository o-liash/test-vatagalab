﻿using System.Collections;
using UnityEngine;
using DG.Tweening;

class DragDrop : MonoBehaviour {
	
	public float interactDistance = 3;
	public float carryDistance = 2.5f;
	public LayerMask mask;

	private Transform carryObject;
	private bool haveObject;
	private static bool isPressedE = false;

	void Update() {
		RaycastHit hit;
		Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
		if (Physics.Raycast(ray, out hit, interactDistance, mask)) {
			Debug.DrawLine(ray.origin, hit.point);
			if (Input.GetKeyDown (KeyCode.E)) {
				Debug.Log ("GetKeyDown (KeyCode.E)");
				isPressedE = !isPressedE;
			}
			if (isPressedE) {
				carryObject = hit.transform;
				haveObject = true;
			}
		}



		if (!isPressedE) {
			if (haveObject) {
				Debug.Log ("haveObject && !isPressedE");
				haveObject = false;
				carryObject.transform.DOMoveY (GameObject.FindGameObjectsWithTag ("wall") [0].GetComponent<Renderer> ().bounds.center.y, 2);
				carryObject.transform.DORotate (new Vector3 (0, 0, 0), 1);
				carryObject = null;
			}
		} 

		if (haveObject) {
			Debug.Log ("haveObject");
			carryObject.GetComponent<Rigidbody> ().MovePosition (Vector3.Lerp (carryObject.position, Camera.main.transform.position + Camera.main.transform.forward * carryDistance, Time.deltaTime * 5));
			carryObject.GetComponent<Rigidbody> ().velocity = Vector3.zero;
		}
	}
} 