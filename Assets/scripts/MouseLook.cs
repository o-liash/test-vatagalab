﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour {

	public enum RotationAxes {
		MouseXandY = 0, MouseX = 1, MouseY = 2
	}

	public RotationAxes axes = RotationAxes.MouseXandY; //this will be shown in Unity editor
	public float sensitivity = 9.0f;
	public float minTilt = -45.0f;
	public float maxTilt = 45.0f;

	private float _rotationX = 0;

	void Start () {
		Rigidbody body = GetComponent<Rigidbody> ();
		if (body != null) {
			body.freezeRotation = true;
		}
	}
	
	void Update () {
		if (axes == RotationAxes.MouseX) {
			// horizontal rotation
			transform.Rotate(0, Input.GetAxis ("Mouse X") * sensitivity, 0);
		} else if (axes == RotationAxes.MouseY) {
			// vertical rotation
			_rotationX -= Input.GetAxis("Mouse Y") * sensitivity;
			_rotationX = Mathf.Clamp (_rotationX, minTilt, maxTilt);
			float rotationY = transform.localEulerAngles.y;
			transform.localEulerAngles = new Vector3 (_rotationX, rotationY, 0);
		} else {
			// multiaxes rotation
			_rotationX -= Input.GetAxis("Mouse Y") * sensitivity;
			_rotationX = Mathf.Clamp (_rotationX, minTilt, maxTilt);

			float delta = Input.GetAxis ("Mouse X") * sensitivity;
			float rotationY = transform.localEulerAngles.y + delta;

			transform.localEulerAngles = new Vector3 (_rotationX, rotationY, 0);
		}
	}
}
